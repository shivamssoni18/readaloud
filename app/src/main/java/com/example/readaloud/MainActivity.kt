@file:Suppress("DEPRECATED_IDENTITY_EQUALS")

package com.example.readaloud

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.speech.tts.TextToSpeech
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.text.TextRecognizer
import com.itextpdf.text.pdf.PdfReader
import com.itextpdf.text.pdf.parser.PdfTextExtractor
import com.teetra.util.FilePath
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.io.FileReader
import java.io.IOException
import java.util.*

class MainActivity : AppCompatActivity(), TextToSpeech.OnInitListener {

    private var tts: TextToSpeech? = null
    private var buttonSpeak: Button? = null
    private var buttonStop: Button? = null
    private var buttonupload: Button? = null
    private var buttonClear: Button? = null
    private var editText: EditText? = null

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        GrantPermission()

        buttonSpeak = this.button_speak
        buttonStop = this.button_stop
        editText = this.edittext_input
        buttonupload = this.btnupload
        buttonClear = this.btnclear

        buttonSpeak!!.isEnabled = false;
        tts = TextToSpeech(this, this)

        buttonSpeak!!.setOnClickListener {

            speakOut()
            buttonStop?.setVisibility(VISIBLE)
        }
        buttonStop!!.setOnClickListener {

            speakStop()
            buttonStop?.setVisibility(INVISIBLE)

        }

        buttonClear!!.setOnClickListener {
            editText?.setText("")
            speakStop()
            buttonStop?.setVisibility(INVISIBLE)
        }

        buttonupload!!.setOnClickListener {
            val intent = Intent()
                .setType("*/*")
                .setAction(Intent.ACTION_GET_CONTENT)

            startActivityForResult(Intent.createChooser(intent, "Select a file"), 111)
        }


    }


    private fun GrantPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) !== PackageManager.PERMISSION_GRANTED && checkSelfPermission(
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 0
            );
        } else {
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            GrantPermission()

        } else {

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 111 && resultCode == RESULT_OK) {
            val selectedFile = data?.data

            Log.e("TTS", "selectedfile " + selectedFile)

            var filePath: String? = null

            if (selectedFile != null) {
                filePath = FilePath.getPath(this, selectedFile)

                Log.e("TTS", "filepath " + filePath)
            }
            val FileExtension = filePath?.substring(filePath.lastIndexOf(".") + 1); // Without dot jpg, png

            if (FileExtension.equals("txt", ignoreCase = true) || FileExtension.equals(
                    "json",
                    ignoreCase = true
                ) || FileExtension.equals("doc", ignoreCase = true)
            ) {
                val textfile = StringBuilder()
                try {
                    val br = BufferedReader(FileReader(filePath))
                    Log.e("TTS", "filepath text " + filePath)
                    var line: String?

                    line = br.readLine()
                    textfile.append(line)

                    editText!!.setText(textfile)
                    br.close()
                    if (textfile.equals("")) {
                        Toast.makeText(applicationContext, "No text found in selected file.", Toast.LENGTH_LONG).show()
                    }

                } catch (e: IOException) {
                    e.printStackTrace()
                }

            } else if (FileExtension.equals("pdf", ignoreCase = true)) {
                try {
                    var parsedText = ""
                    val reader = PdfReader(filePath)
                    Log.e("TTS", "filepath pdf " + filePath)
                    val n = reader.getNumberOfPages()
                    for (i in 0 until n) {
                        parsedText = parsedText + PdfTextExtractor.getTextFromPage(
                            reader,
                            i + 1
                        ).trim { it <= ' ' } + "\n" //Extracting the content from the different pages
                    }

                    editText!!.setText(parsedText)
                    if (parsedText == "") {

                        Toast.makeText(applicationContext, "No text found in selected file.", Toast.LENGTH_LONG).show()
                    }

                    reader.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            } else if (FileExtension.equals("jpeg", ignoreCase = true) || FileExtension.equals(
                    "jpg",
                    ignoreCase = true
                ) || FileExtension.equals("png", ignoreCase = true)
            ) {
                try {

                    val uri = Uri.parse(selectedFile.toString())
                    val bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                    val textRecognizer = TextRecognizer.Builder(applicationContext).build()
                    if (!textRecognizer.isOperational) {
                        Toast.makeText(this, "no data", Toast.LENGTH_SHORT).show()
                    } else {

                        val frame = Frame.Builder().setBitmap(bitmap).build()

                        val items = textRecognizer.detect(frame)

                        val sb = StringBuilder()

                        for (i in 0 until items.size()) {

                            val myItem = items.valueAt(i)
                            sb.append(myItem.value)
                            sb.append("\n")

                        }
                        editText!!.setText(sb.toString())
                        if (sb.toString().equals("")) {
                            Toast.makeText(applicationContext, "No text found in selected file.", Toast.LENGTH_LONG)
                                .show()
                        }

                    }

                } catch (e: IOException) {
                    e.printStackTrace()
                }
            } else {

                Toast.makeText(applicationContext, "file not supported", Toast.LENGTH_LONG).show()

            }


        }
    }


    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            // set US English as language for tts
            val result = tts!!.setLanguage(Locale.US)

            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "The Language specified is not supported!")
            } else {
                buttonSpeak!!.isEnabled = true
            }

        } else {
            Log.e("TTS", "Initilization Failed!")
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun speakOut() {
        val text = editText!!.text.toString()
        tts!!.speak(text, TextToSpeech.QUEUE_FLUSH, null, "")
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun speakStop() {
        tts!!.stop()
    }


    public override fun onDestroy() {
        // Shutdown TTS
        if (tts != null) {
            tts!!.stop()
            tts!!.shutdown()
        }
        super.onDestroy()
    }
}